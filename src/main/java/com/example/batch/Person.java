package com.example.batch;

public class Person {

	private String name;

	private String paternal;

	private String maternal;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPaternal() {
		return paternal;
	}

	public void setPaternal(String paternal) {
		this.paternal = paternal;
	}

	public String getMaternal() {
		return maternal;
	}

	public void setMaternal(String maternal) {
		this.maternal = maternal;
	}

	@Override
	public String toString() {
		StringBuilder b = new StringBuilder();
		b.append(name);
		b.append(',').append(paternal);
		b.append(',').append(maternal);
		return b.toString();
	}

}