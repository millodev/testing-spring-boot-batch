**testing-spring-boot-batch**

Just a simple batch processing, read a **persons.csv** file, that contains three columns, **name**, **paternal**, **maternal** and print it out to the console.

**application.yml**
change the values of database, user, password into your local mysql database.

Mininal version:

- Java 11
- Gradle 7.2
- Spring Boot 2.5.5
- Spring Batch
- MySql Connector (need it for run bath proccess).





